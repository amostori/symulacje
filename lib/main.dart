// flutter build apk --split-per-abi
// flutter build appbundle
// µ
// ctrl + shift + alt + j - zaznacz wszystkie podobne
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:symulacje/src/adding/addingScreen.dart';
import 'package:symulacje/src/blocs/ekg_provider.dart';
import 'package:symulacje/src/editing/editScreen.dart';
import 'package:symulacje/src/ekg/ekgPresenting.dart';
import 'package:symulacje/src/home/home.dart';
import 'package:symulacje/src/presentingScreen/presentingScreen.dart';
import 'src/blocs/remark_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return EkgProvider(
      child: RemarkProvider(
        child: MaterialApp(
          title: 'Italiano',
          theme: ThemeData(
            primarySwatch: Colors.red,
            accentColor: Colors.amber,
            canvasColor: Color.fromRGBO(255, 254, 229, 1),
            fontFamily: 'Raleway',
            textTheme: ThemeData.light().textTheme.copyWith(
                  body1: TextStyle(
                    color: Color.fromRGBO(20, 51, 51, 1),
                    fontSize: 18,
                  ),
                  body2: TextStyle(
                    fontSize: 18,
                    color: Color.fromRGBO(20, 51, 51, 1),
                  ),
                  title: TextStyle(
                    fontSize: 18,
                    fontFamily: 'RobotoCondensed',
                    fontWeight: FontWeight.bold,
                  ),
                ),
          ),
          initialRoute: Home.id,
          //onGenerateRoute: routes,
          routes: {
            Home.id: (context) => Home(),
            AddingScreen.id: (context) => AddingScreen(),
            PresentingScreen.id: (context) {
              return PresentingScreen();
            },
            EditScreen.id: (context) => EditScreen(),
            EkgPresenting.id: (context) => EkgPresenting(),
          },
        ),
      ),
    );
  }

  Route routes(RouteSettings settings) {
    switch (settings.name) {
      case Home.id:
        return MaterialPageRoute(
          builder: (context) {
            return Home();
          },
        );
      case AddingScreen.id:
        return MaterialPageRoute(
          builder: (context) {
            return AddingScreen();
          },
        );
      case PresentingScreen.id:
        return MaterialPageRoute(
          builder: (context) {
            return PresentingScreen();
          },
        );
      case EditScreen.id:
        return MaterialPageRoute(
          builder: (context) {
            return EditScreen();
          },
        );
      case EkgPresenting.id:
        return MaterialPageRoute(
          builder: (context) {
            return EkgPresenting();
          },
        );
    }
  }
}
