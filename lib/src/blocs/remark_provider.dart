import 'package:flutter/material.dart';
import 'bloc.dart';

class RemarkProvider extends InheritedWidget {
  final bloc = Bloc();
  RemarkProvider({Key key, Widget child}) : super(key: key, child: child);
  static Bloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(RemarkProvider)
            as RemarkProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    // TODO: implement updateShouldNotify
    return true;
  }
}
