import 'package:rxdart/rxdart.dart';

class Bloc extends Object {
  final _remarkController = BehaviorSubject<String>();

  Observable<String> get remark => _remarkController.stream;

  Function(String) get changeRemark => _remarkController.sink.add;

  addRemark(Function function) {
    String remark = _remarkController.value;
    function(remark);
  }

  dispose() {
    _remarkController.close();
  }
}
