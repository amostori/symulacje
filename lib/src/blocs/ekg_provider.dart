import 'package:flutter/material.dart';
import 'bloc.dart';

class EkgProvider extends InheritedWidget {
  final bloc = Bloc();
  EkgProvider({Key key, Widget child}) : super(key: key, child: child);
  static Bloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(EkgProvider) as EkgProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    // TODO: implement updateShouldNotify
    return true;
  }
}
