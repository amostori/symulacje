import 'dart:async';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import '../models/simulationModel.dart';

class DBHelper {
  static Future<sql.Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, 'simulations.db'),
        onCreate: (db, version) {
      return db.execute('CREATE TABLE simulations'
          '(id INTEGER PRIMARY KEY '
          'AUTOINCREMENT, '
          'title TEXT, '
          'call TEXT,'
          'arrival TEXT,'
          'avpu TEXT,'
          'breathing TEXT,'
          'circulation TEXT,'
          'parameters TEXT,'
          'modifications TEXT,'
          'strategy TEXT,'
          'mistake TEXT,'
          'symptoms TEXT,'
          'alergy TEXT,'
          'meds TEXT,'
          'pasthistory TEXT,'
          'lunch TEXT,'
          'events TEXT,'
          'ekg TEXT'
          ')');
    }, version: 1);
  }

  static Future<void> insert(Map<String, Object> data) async {
    final db = await DBHelper.database();
    db.insert(
      'simulations',
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<SimulationModel>> getData() async {
    final db = await DBHelper.database();
    final List<Map<String, dynamic>> maps = await db.query('simulations');
    return maps.length == 0
        ? null
        : List.generate(maps.length, (index) {
            return SimulationModel(
              id: maps[index]['id'],
              title: maps[index]['title'],
              call: maps[index]['call'],
              arrival: maps[index]['arrival'],
              avpu: maps[index]['avpu'],
              breathing: maps[index]['breathing'],
              circulation: maps[index]['circulation'],
              parameters: maps[index]['parameters'],
              modifications: maps[index]['modifications'],
              strategy: maps[index]['strategy'],
              mistake: maps[index]['mistake'],
              symptoms: maps[index]['symptoms'],
              alergy: maps[index]['alergy'],
              meds: maps[index]['meds'],
              pastHistory: maps[index]['pasthistory'],
              lunch: maps[index]['lunch'],
              events: maps[index]['events'],
              ekg: maps[index]['ekg'],
            );
          });
  }

  static Future<void> deleteSimulation(String text) async {
    final db = await DBHelper.database();
    await db.delete('simulations', where: "title= ?", whereArgs: [text]);
  }

  static Future<void> updateSentence(SimulationModel simulation) async {
    final db = await DBHelper.database();
    await db.update('simulations', simulation.toMap(),
        where: "id= ?", whereArgs: [simulation.id]);
  }
}
