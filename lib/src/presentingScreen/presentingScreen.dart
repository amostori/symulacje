import 'package:flutter/material.dart';
import 'package:symulacje/constance.dart';
import 'package:symulacje/src/blocs/bloc.dart';
import 'package:symulacje/src/blocs/ekg_provider.dart';
import 'package:symulacje/src/blocs/remark_provider.dart';
import 'package:symulacje/src/ekg/ekgPresenting.dart';
import 'package:symulacje/src/home/home.dart';
import 'package:symulacje/src/models/simulationModel.dart';

class PresentingScreen extends StatefulWidget {
  static const String id = 'PresentingScreen';

  final SimulationModel simulationModel;
  PresentingScreen({this.simulationModel});

  @override
  _PresentingScreenState createState() => _PresentingScreenState();
}

class _PresentingScreenState extends State<PresentingScreen> {
  List<String> simulationList = List();
  TextEditingController controller = TextEditingController();
  ScrollController _scrollController = ScrollController();
  String remark = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.simulationModel.toMap().forEach((key, value) {
      if (!key.contains('id')) {
        simulationList.add(value);
      }
    });
    simulationList.add('');
  }

  @override
  Widget build(BuildContext context) {
    Bloc bloc = RemarkProvider.of(context);
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/background2.png',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color.fromRGBO(0, 0, 0, 0.2),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text(
              widget.simulationModel.title,
              style: TextStyle(color: Colors.white70),
            ),
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.transparent,
          body: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: ListView.builder(
              controller: _scrollController,
              itemBuilder: (context, index) {
                Color fontColor;
                if (index < 16) {
                  fontColor = Colors.white;
                } else {
                  fontColor = Colors.yellow;
                }
                if (index == 12) {
                  return ListTile(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            print(
                                'simulationList index = ${simulationList[index + 1]}');

                            return EkgPresenting(
                                ekg: '${simulationList[index + 1]}');
                          },
                        ),
                      );
                    },
                    title: Text(
                      'EKG ${simulationList[index + 1]} - kliknij',
                      style: TextStyle(color: fontColor),
                    ),
                  );
                }
                return ListTile(
                  title: Text(
                    '${firstWords[index]} '
                    '${simulationList[index + 1]}',
                    style: TextStyle(color: fontColor),
                  ),
                );
              },
              itemCount: simulationList.length - 1,
            ),
          ),
          floatingActionButton: FloatingActionButton.extended(
            onPressed: () => _displayDialog(context, bloc),
            elevation: 0.0,
            label: Text(
              'Uwaga',
              style: TextStyle(color: Colors.white70, fontSize: 24),
            ),
            icon: Icon(
              Icons.add,
              color: Colors.white70,
              size: 24,
            ),
            backgroundColor: Color.fromRGBO(0, 0, 0, 0.7),
          ),
        ),
      ],
    );
  }

  void addHint(String text) {
    simulationList.add(text);
    firstWords.add('');
    _scrollController.animateTo(_scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
    setState(() {});
  }

  _displayDialog(BuildContext context, Bloc bloc) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Uwaga instruktora'),
            content: TextField(
              autofocus: true,
              onChanged: (text) {
                remark = text;
              },
              onSubmitted: (text) {
                remark = text;
                addHint(remark);
                Navigator.of(context).pop();
              },
              controller: controller,
              decoration: InputDecoration(hintText: 'Uwaga instruktora'),
            ),
            actions: <Widget>[
              StreamBuilder<String>(
                  stream: bloc.remark,
                  builder: (context, snapshot) {
                    return FlatButton(
                      child: Text(
                        'Zapisz',
                        style: TextStyle(color: Colors.indigo),
                      ),
                      onPressed: () {
                        bloc.changeRemark(controller.text);
                        bloc.addRemark(addHint);
                        // addHint(controller.text);
                        Navigator.of(context).pop();
                      },
                    );
                  })
            ],
          );
        });
  }
}
