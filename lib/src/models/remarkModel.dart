class RemarkModel {
  int id;
  String remark;

  RemarkModel({
    this.id,
    this.remark,
  });
  Map<String, dynamic> toMap() => {
        'id': id,
        'remark': remark ?? '',
      };
}
