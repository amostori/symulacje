class SimulationModel {
  int id;
  String title;
  String call;
  String arrival;
  String avpu;
  String breathing;
  String circulation;
  String parameters;
  String ekg;
  String modifications;
  String strategy;
  String mistake;
  String symptoms;
  String alergy;
  String meds;
  String pastHistory;
  String lunch;
  String events;

  SimulationModel({
    this.id,
    this.title,
    this.call,
    this.arrival,
    this.avpu,
    this.breathing,
    this.circulation,
    this.symptoms,
    this.alergy,
    this.meds,
    this.pastHistory,
    this.lunch,
    this.events,
    this.parameters,
    this.ekg,
    this.modifications,
    this.strategy,
    this.mistake,
  });
  Map<String, dynamic> toMap() => {
        'id': id,
        'title': title ?? 'Nie wpisałeś tytułu',
        'call': call ?? 'brak danych',
        'arrival': arrival ?? 'brak danych',
        'avpu': avpu ?? 'brak danych',
        'breathing': breathing ?? 'brak danych',
        'circulation': circulation ?? 'brak danych',
        'symptoms': symptoms ?? 'brak danych',
        'alergy': alergy ?? 'brak danych',
        'meds': meds ?? 'brak danych',
        'pastHistory': pastHistory ?? 'brak danych',
        'lunch': lunch ?? 'brak danych',
        'events': events ?? 'brak danych',
        'parameters': parameters ?? 'Pozostałe parametry w normie',
        'ekg': ekg ?? 'placeholder',
        'modifications': modifications ?? 'brak danych',
        'strategy': strategy ?? 'brak danych',
        'mistake': mistake ?? 'brak danych',
      };
}
