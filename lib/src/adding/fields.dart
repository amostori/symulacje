import 'package:flutter/material.dart';

String title;

String call;

String arrival;

String avpu;

String breathing;

String circulation;

String parameters;

String modifications;

String strategy;

String mistake;
String symptoms;
String alergy;
String meds;
String past;
String lunch;
String events;
String ekg;

final TextEditingController titleController = TextEditingController();

final TextEditingController callController = TextEditingController();

final TextEditingController arrivalController = TextEditingController();

final TextEditingController avpuController = TextEditingController();

final TextEditingController breathingController = TextEditingController();

final TextEditingController circulationController = TextEditingController();

final TextEditingController parametersController = TextEditingController();

final TextEditingController modificationsController = TextEditingController();

final TextEditingController strategyController = TextEditingController();

final TextEditingController mistakeController = TextEditingController();
final TextEditingController symptomsController = TextEditingController();
final TextEditingController alergyController = TextEditingController();
final TextEditingController medsController = TextEditingController();
final TextEditingController pastController = TextEditingController();
final TextEditingController lunchController = TextEditingController();
final TextEditingController eventsController = TextEditingController();
final TextEditingController ekgController = TextEditingController();
FocusNode nodeCall = FocusNode();
FocusNode nodeArrival = FocusNode();
FocusNode nodeAvpu = FocusNode();
FocusNode nodeBreathing = FocusNode();
FocusNode nodeCirculation = FocusNode();
FocusNode nodeParameters = FocusNode();
FocusNode nodeModifications = FocusNode();
FocusNode nodeStrategy = FocusNode();
FocusNode nodeMistake = FocusNode();
FocusNode nodeSymptoms = FocusNode();
FocusNode nodeAlergy = FocusNode();
FocusNode nodeMeds = FocusNode();
FocusNode nodePastHistory = FocusNode();
FocusNode nodeLunch = FocusNode();
FocusNode nodeEvents = FocusNode();
FocusNode nodeEkg = FocusNode();
