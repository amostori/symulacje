import 'package:flutter/material.dart';
import 'package:symulacje/constance.dart';
import 'package:symulacje/src/blocs/bloc.dart';
import 'package:symulacje/src/blocs/ekg_provider.dart';
import 'package:symulacje/src/blocs/remark_provider.dart';
import 'package:symulacje/src/db/dbHelper.dart';
import 'package:symulacje/src/home/home.dart';
import 'package:symulacje/src/models/simulationModel.dart';
import 'fields.dart' as fields;

class AddingScreen extends StatefulWidget {
  static const String id = 'AddingScreen';
  @override
  _AddingScreenState createState() => _AddingScreenState();
}

class _AddingScreenState extends State<AddingScreen> {
  @override
  Widget build(BuildContext context) {
    Bloc ekgBloc = EkgProvider.of(context);
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/background.png',
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: ListView(
            children: <Widget>[
              getTitleInput(),
              getCallInput(),
              getArrivalInput(),
              getAvpuInput(),
              getBreathingInput(),
              getCirculationInput(),
              getSymptomsInput(),
              getAlergyInput(),
              getMedsInput(),
              getPastInput(),
              getLunchInput(),
              getEventsInput(),
              getParametersInput(),
              getEkgInput(ekgBloc),
              getModificationsInput(),
              getStrategyInput(),
              getMistakeInput(),
              getSubmitButton(),
            ],
          ),
        ),
      ],
    );
  }

  Widget getTitleInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            autofocus: true,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Tytuł scenki',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.titleController,
            onChanged: (text) {
              setState(() {
                fields.title = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeCall);
              setState(() {
                fields.title = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getCallInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeCall,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Opis wezwania',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.callController,
            onChanged: (text) {
              setState(() {
                fields.call = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeArrival);
              setState(() {
                fields.call = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getArrivalInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeArrival,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Po przybyciu',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.arrivalController,
            onChanged: (text) {
              setState(() {
                fields.arrival = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeAvpu);
              setState(() {
                fields.arrival = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getAvpuInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeAvpu,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Przytomność',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.avpuController,
            onChanged: (text) {
              setState(() {
                fields.avpu = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeBreathing);
              setState(() {
                fields.avpu = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getBreathingInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeBreathing,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Ocena oddechu',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.breathingController,
            onChanged: (text) {
              setState(() {
                fields.breathing = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeCirculation);
              setState(() {
                fields.breathing = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getCirculationInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeCirculation,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Ocena krążenia',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.circulationController,
            onChanged: (text) {
              setState(() {
                fields.circulation = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeSymptoms);
              setState(() {
                fields.circulation = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getSymptomsInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeSymptoms,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Wywiad - objawy',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.symptomsController,
            onChanged: (text) {
              setState(() {
                fields.symptoms = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeAlergy);
              setState(() {
                fields.symptoms = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getAlergyInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeAlergy,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Wywiad - alergie',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.alergyController,
            onChanged: (text) {
              setState(() {
                fields.alergy = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeMeds);
              setState(() {
                fields.alergy = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getMedsInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeMeds,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Wywiad - medykamenty',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.medsController,
            onChanged: (text) {
              setState(() {
                fields.meds = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodePastHistory);
              setState(() {
                fields.meds = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getPastInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodePastHistory,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Wywiad - przeszłość chorobowa',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.pastController,
            onChanged: (text) {
              setState(() {
                fields.past = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeLunch);
              setState(() {
                fields.past = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getLunchInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeLunch,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Wywiad - lunch',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.lunchController,
            onChanged: (text) {
              setState(() {
                fields.lunch = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeEvents);
              setState(() {
                fields.lunch = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getEventsInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeEvents,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Wywiad - zdarzenia',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.eventsController,
            onChanged: (text) {
              setState(() {
                fields.events = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeParameters);
              setState(() {
                fields.events = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getParametersInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeParameters,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Pozostałe parametry',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.parametersController,
            onChanged: (text) {
              setState(() {
                fields.parameters = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeEkg);
              setState(() {
                fields.parameters = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getEkgInput(Bloc ekgBloc) {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeEkg,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Wpisz nazwę ekg',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.ekgController,
            onChanged: (text) {
              setState(() {
                fields.ekg = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeModifications);
              setState(() {
                fields.ekg = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getModificationsInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeModifications,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Zmiany w czasie',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.modificationsController,
            onChanged: (text) {
              setState(() {
                fields.modifications = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeStrategy);
              setState(() {
                fields.modifications = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getStrategyInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeStrategy,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Najważniejsze interwencje - diagnoza',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.strategyController,
            onChanged: (text) {
              setState(() {
                fields.strategy = text;
              });
            },
            onSubmitted: (text) {
              FocusScope.of(context).requestFocus(fields.nodeMistake);
              setState(() {
                fields.strategy = text;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget getMistakeInput() {
    return Padding(
      padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
      child: Card(
        color: kTextFieldBackground,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: TextField(
            focusNode: fields.nodeMistake,
            maxLines: null,
            style: TextStyle(fontSize: 28, color: Colors.white70),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
              hintText: 'Błędy krytyczne',
              hintStyle: TextStyle(color: Colors.white38),
            ),
            controller: fields.mistakeController,
            onChanged: (text) {
              setState(() {
                fields.mistake = text;
              });
            },
            onSubmitted: (text) {
              setState(() {
                fields.mistake = text;
              });
              SimulationModel model = SimulationModel(
                title: fields.title,
                call: fields.call,
                arrival: fields.arrival,
                avpu: fields.avpu,
                breathing: fields.breathing,
                circulation: fields.circulation,
                parameters: fields.parameters,
                modifications: fields.modifications,
                strategy: fields.strategy,
                mistake: fields.mistake,
                symptoms: fields.symptoms,
                alergy: fields.alergy,
                meds: fields.meds,
                pastHistory: fields.past,
                lunch: fields.lunch,
                events: fields.events,
              );
              addToDatabase(model);
              Navigator.pushReplacementNamed(context, Home.id);
            },
          ),
        ),
      ),
    );
  }

  Widget getSubmitButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 64),
      child: RaisedButton(
        elevation: 0,
        color: kTextFieldBackground,
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 18),
        textColor: Colors.white70,
        onPressed: () {
          SimulationModel model = SimulationModel(
            title: fields.title,
            call: fields.call,
            arrival: fields.arrival,
            avpu: fields.avpu,
            breathing: fields.breathing,
            circulation: fields.circulation,
            parameters: fields.parameters,
            ekg: fields.ekg,
            modifications: fields.modifications,
            strategy: fields.strategy,
            mistake: fields.mistake,
            symptoms: fields.symptoms,
            alergy: fields.alergy,
            meds: fields.meds,
            pastHistory: fields.past,
            lunch: fields.lunch,
            events: fields.events,
          );
          addToDatabase(model);
          Navigator.pushReplacementNamed(context, Home.id);
        },
        child: Text(
          'Zapisz',
          style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  void addToDatabase(SimulationModel simulationModel) async {
    await DBHelper.insert(simulationModel.toMap());
  }
}
