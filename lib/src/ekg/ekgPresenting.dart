import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'dart:io';

class EkgPresenting extends StatefulWidget {
  static const String id = 'EkgPresenting';
  final String ekg;
  EkgPresenting({this.ekg});

  @override
  _EkgPresentingState createState() => _EkgPresentingState();
}

class _EkgPresentingState extends State<EkgPresenting> {
  String ekgName;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ekgName = widget.ekg;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/background.png',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color.fromRGBO(0, 0, 0, 0.2),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text(
              'Zapis ekg $ekgName',
              style: TextStyle(color: Colors.white70),
            ),
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.transparent,
          body: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: RefreshIndicator(
              onRefresh: () async {
                setState(() {
                  print('set state done');
                });
              },
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 50,
                  ),
                  Container(
                    child: FutureBuilder(
                      future: showMyImage(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return snapshot.data;
                        } else {
                          return CircularProgressIndicator();
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Color.fromRGBO(0, 0, 0, 0.5),
            onPressed: () {
              setState(() {});
            },
            child: Icon(
              Icons.refresh,
              color: Colors.white70,
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
          bottomNavigationBar: BottomAppBar(
            color: Colors.black26,
            shape: CircularNotchedRectangle(),
            notchMargin: 4,
            child: Container(
              child: InkWell(
                onTap: () {
                  setState(() {});
                },
                child: Center(
                  child: Text(
                    'Odśwież',
                    style: TextStyle(color: Colors.white70),
                  ),
                ),
              ),
              height: 50,
            ),
          ),
        ),
      ],
    );
  }

  showMyImage() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return FadeInImage.memoryNetwork(
            placeholder: kTransparentImage,
            image: 'http://kursandroida.pl/$ekgName.jpg');
      }
    } on SocketException catch (_) {
      return Center(
          child: Text(
        'Brak połączenia z internetem.',
        style: TextStyle(color: Colors.white70),
      ));
    }
  }

  /*showMyEkg() async {
    String ekgFromInternet;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        ekgFromInternet = 'http://kursandroida.pl/$ekgName.jpg';
        return ekgFromInternet;
      }
    } on SocketException catch (_) {
      return 'not connected';
    }
  }*/
}
