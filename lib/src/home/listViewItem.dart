import 'package:flutter/material.dart';
import 'package:symulacje/src/models/simulationModel.dart';
import 'package:symulacje/src/presentingScreen/presentingScreen.dart';

class ListViewItem extends StatelessWidget {
  final String title;
  final SimulationModel simulationModel;
  ListViewItem({this.title, this.simulationModel});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => PresentingScreen(
            simulationModel: simulationModel,
          ),
        ),
      ),
      child: Card(
        color: Colors.transparent,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              title,
              style: TextStyle(fontSize: 28, color: Colors.white70),
            ),
          ),
        ),
      ),
    );
  }
}
