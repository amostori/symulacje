import 'package:flutter/material.dart';
import 'package:symulacje/src/adding/addingScreen.dart';
import 'package:symulacje/src/db/dbHelper.dart';
import 'package:symulacje/src/editing/editScreen.dart';
import 'package:symulacje/src/models/simulationModel.dart';
import 'listViewWidget.dart';

class Home extends StatefulWidget {
  static const String id = 'Home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/background.png',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color.fromRGBO(0, 0, 0, 0.2),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text(
              'Symulacje ratownicze',
              style: TextStyle(color: Colors.white70),
            ),
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.transparent,
          body: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: FutureBuilder<List<SimulationModel>>(
              future: DBHelper.getData(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<SimulationModel>> snapshot) {
                if (snapshot.hasData) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 50),
                    child:
                        createListView(snapshot.data, updateItem, deleteItem),
                  );
                } else {
                  return Center(
                    child: GestureDetector(
                      onTap: goToAddingScreen,
                      child: Center(
                        child: Text(
                          '+',
                          style:
                              TextStyle(fontSize: 300, color: Colors.white70),
                        ),
                      ),
                    ),
                  );
                }
              },
            ),
          ),
          floatingActionButton: FloatingActionButton.extended(
            onPressed: goToAddingScreen,
            elevation: 0.0,
            label: Text(
              'Dodaj',
              style: TextStyle(color: Colors.white70, fontSize: 24),
            ),
            icon: Icon(
              Icons.add,
              color: Colors.white70,
              size: 24,
            ),
            backgroundColor: Color.fromRGBO(0, 0, 0, 0.7),
          ),
        ),
      ],
    );
  }

  void updateItem(SimulationModel item) async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => EditScreen(
          simulationModel: item,
        ),
      ),
    );
  }

  void deleteItem(String item) async {
    await DBHelper.deleteSimulation(item);
    setState(() {});
    Navigator.pushReplacementNamed(context, Home.id);
    // printDatabase();
  }

  void goToAddingScreen() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => AddingScreen(),
      ),
    );
  }
}
