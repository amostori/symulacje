import 'package:flutter/material.dart';
import 'package:symulacje/constance.dart';
import 'package:symulacje/src/home/listViewItem.dart';
import 'package:symulacje/src/models/simulationModel.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

Widget createListView(
    List<SimulationModel> simulationList, Function update, Function delete) {
  return ListView.builder(
    itemCount: simulationList.length,
    itemBuilder: (BuildContext context, int index) {
      SimulationModel item = simulationList[index];
      return Slidable(
        actionPane: SlidableDrawerActionPane(),
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Edytuj',
            color: kEditColor,
            icon: Icons.edit,
            onTap: () => update(item),
          ),
          IconSlideAction(
            caption: 'Usuń',
            color: kDeleteColor,
            icon: Icons.delete,
            onTap: () => delete(item.title),
          ),
        ],
        child: ListViewItem(
          title: item.title,
          simulationModel: item,
        ),
      );
    },
  );
}
