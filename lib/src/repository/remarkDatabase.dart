import 'dart:async';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import 'package:symulacje/src/models/remarkModel.dart';

class RemarkDB {
  static Future<sql.Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, 'remarks.db'),
        onCreate: (db, version) {
      return db.execute('CREATE TABLE remarks'
          '(id INTEGER PRIMARY KEY '
          'AUTOINCREMENT, '
          'remark TEXT, '
          ')');
    }, version: 1);
  }

  static Future<void> insert(Map<String, Object> data) async {
    final db = await RemarkDB.database();
    db.insert(
      'remarks',
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<RemarkModel>> getData() async {
    final db = await RemarkDB.database();
    final List<Map<String, dynamic>> maps = await db.query('remarks');
    return maps.length == 0
        ? null
        : List.generate(maps.length, (index) {
            return RemarkModel(
              id: maps[index]['id'],
              remark: maps[index]['remark'],
            );
          });
  }

  static Future<void> deleteSimulation(String text) async {
    final db = await RemarkDB.database();
    await db.delete('remarks', where: "remark= ?", whereArgs: [text]);
  }
}
